<?php

ini_set("display_errors", '1');
//echo ini_get('display_errors');
error_reporting(E_ALL);
header("Access-Control-Allow-Origin: *");
header('Content-type:application/json;charset=utf-8');
// header('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE');
// header('Access-Control-Allow-Headers', 'Content-Type,Accept');
require __DIR__ . '/vendor/autoload.php';

$action = $_GET['action'];
$uri = null;
switch($action){
    case 'place_details':
        $place_id = $_GET['place_id'];
        $uri = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.  $place_id;
    break;
}
if(!$uri){
    die;
}
// $uri='https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJ96FbyHonHRURAdNZ9VGpx0Q&key=AIzaSyDz_aQ2_PGAvoALbH9oHtg-YpmxmH1K0lg';
$apiKey = "AIzaSyDz_aQ2_PGAvoALbH9oHtg-YpmxmH1K0lg";
// echo $uri;
$response = \Httpful\Request::get($uri . '&key='.$apiKey)
->sendsJson()
->send();
// print_r(json_encode($response));

echo json_encode($response->body);
die;

?>