import * as Actions from '../actions/Actions'
let path = '/httpService.php';
let origin = window.location.origin;

export const DEV_MODE = window.location.hostname === 'localhost' ? true : false;
if (DEV_MODE) {
  path = '/projects/reactjs/sabbathAppReact/httpService.php';
  origin = origin.replace(3000, 8080);
}
export const BASE_URL = origin + path;
const initState = {
    baseUrl: BASE_URL,
    placeList: [],
    placeListReports: [],
    markerList: [],
    placeName: '',
    newPlaceDetails: null,
    showSuccessModal: false,
    displayInMap: false,
}
function Reducers(state = initState, action) {
    let newState = { ...state };
    // console.log(action);
    switch (action.type) {
        case Actions.Types.SEARCH_FOR_PALCE:
            newState.placeList = action.placeList;
            newState.placeName = action.placeName;
            break
        case Actions.Types.DISPLAY_PLACE_IN_MAP:
            let currentPlaceList, placeTypeName, placeId;
            if (action.placeType == 'google') {
                currentPlaceList = [...newState.placeList];
                placeTypeName = 'placeList';
                placeId = 'id';

            } else {
                currentPlaceList = [...newState.placeListReports];
                placeTypeName = 'placeListReports';
                placeId = 'place_id';
            }
            // console.log(action);
            for (let i in currentPlaceList) {
                if (currentPlaceList[i][placeId] === action.place[placeId]) {
                    currentPlaceList[i].displayInMap = !currentPlaceList[i].displayInMap;
                }
            }
            newState[placeTypeName] = currentPlaceList
            newState.displayInMap = true;
            break
        case Actions.Types.SET_PLACE_ERROR:
            let placeList = [...newState.placeList];
            for (let i in placeList) {
                if (placeList[i].id === action.place.id) {
                    placeList[i] = { ...placeList[i] }
                    placeList[i].error = action.error;
                }
            }
            break
        case Actions.Types.CLEAN_MARKER_LIST:
            newState.markerList = [];
            break
        case Actions.Types.UPDATE_MARKER_LIST:
            newState.markerList = action.markerList;
            newState.displayInMap = false;
            break
        case Actions.Types.FETCH_ALL_PLACES:
            newState.placeListReports = action.placeListReports || [];
            break
        case Actions.Types.SHOW_SUCCESS_MODAL:
            newState.showSuccessModal = action.showModal;
            newState.newPlaceDetails = action.newPlaceDetails;
            break
        default:
    }
    return newState

}


export default Reducers;