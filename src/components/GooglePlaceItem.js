import React from 'react';
import axios from 'axios';

export default class GooglePlaceItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            opening_hours: null,
            openInSaturday: 'אין מידע',
            openNow: 'אין מידע',
            error: false
        };
        this.winterMonths = [11, 12, 1, 2];
        this.middleMonths = [3, 4, 9, 10];
        this.summerMonths = [5, 6, 7, 8];
    }
    componentWillMount() {
        let self = this;
        if (!this.props.place.opening_hours) {
            return;
        }
        // console.log(this.props.place.opening_hours);
        if (!this.props.place.opening_hours.weekday_text ||
            this.props.place.opening_hours.weekday_text.length === 0) {
            setTimeout(function () {
                self.getOpeningHours(self.props.place)
            }, this.props.index * 200)
        } else {
            this.setOpeningHours(this.props.place.opening_hours);
        }
    }
    getOpeningHours(place) {
        // console.log(place.id);
        let self = this;
        axios({
            url: this.props.baseUrl,
            method: 'GET',
            params: { place_id: place.place_id, action: 'place_details' }
        }).then(function (resp) {
            // console.log(resp.data.result);
            self.props.setPlaceError(place, false);
            if (resp.data.result, resp.data.result.opening_hours) {
                let opening_hours = resp.data.result.opening_hours;
                self.setOpeningHours(opening_hours)
            }
        }, function (error) {
            self.setState({ error: true })
            self.props.setPlaceError(place, true);
        });
    }
    setOpeningHours(opening_hours) {
        let openInSaturdayHours = undefined;
        let closeInSaturdayStatus = undefined;
        let open_now = 'אין מידע';
        if (opening_hours && opening_hours.periods) {
            // console.log(opening_hours.weekday_text, opening_hours.periods);
            let fridayTextIndex = 5;
            let saturdayTextIndex = 6;
            if (opening_hours.weekday_text.length == 7) {
                fridayTextIndex = 4;
                saturdayTextIndex = 5;
            }
            // console.log(this.props.place.name, opening_hours);
            let closeInSaturday = this.checkIfOpenInFriday(opening_hours.periods, opening_hours.weekday_text[fridayTextIndex]);
            let closeInFriday = this.checkIfOpenInSaturday(opening_hours.periods, opening_hours.weekday_text[saturdayTextIndex]);
            if (opening_hours.periods.length == 1 && opening_hours.periods[0].close == undefined) {
                closeInSaturday = false;
            }
            // console.log(closeInSaturday, closeInFriday);
            closeInSaturdayStatus = (closeInSaturday == true && closeInFriday == true) ? true : false;
            open_now = opening_hours.openNow ? 'פתוח' : 'סגור';
            // console.log(closeInSaturdayStatus, closeInSaturday, closeInFriday);
        }
        this.setState({
            opening_hours: opening_hours,
            closeInSaturdayStatus: closeInSaturdayStatus,
            openInSaturdayHours: openInSaturdayHours,
            openNow: open_now,
            error: false
        });
    }
    checkIfOpenInSaturday(openingHoursData, openInSaturdayHours) {
        if (openInSaturdayHours == 'Saturday: Closed' || openInSaturdayHours == 'יום שבת: סגור') {
            return true;
        } else if (openInSaturdayHours == 'Saturday: Open 24 hours' || openInSaturdayHours == 'Saturday: פתוח 24 שעות') {
            return false;
        }
        let openHours = this.getOpenHours(openingHoursData, 6) //Check if sabbath exist in array

        if (openHours) {
            let d = new Date();
            let currentMonth = d.getMonth() + 1;
            let testMinTime = true;
            let isClose = false;

            if (this.winterMonths.indexOf(currentMonth) != -1) {
                isClose = this.checkHour(openHours, 18, testMinTime)
            } else if (this.middleMonths.indexOf(currentMonth) != -1) {
                isClose = this.checkHour(openHours, 19, testMinTime)
            } else if (this.summerMonths.indexOf(currentMonth) != -1) {
                isClose = this.checkHour(openHours, 20, testMinTime)
            }
            return isClose;
        }

        return true;


    }
    checkIfOpenInFriday(openingHoursData, openInFridayHours) {
        // console.log(openingHoursData, openInFridayHours);

        if (openInFridayHours == 'Friday: Closed' || openInFridayHours == 'יום שישי: סגור') {
            return true;
        } else if (openInFridayHours == 'Friday: Open 24 hours' || openInFridayHours == 'Friday: פתוח 24 שעות') {
            return false;
        }
        let openHours = this.getOpenHours(openingHoursData, 5); //Check if friday exist in array
        if (openHours) {
            let d = new Date();
            let currentMonth = d.getMonth() + 1;
            let isClose = false;
            if (this.winterMonths.indexOf(currentMonth) != -1) {
                isClose = this.checkHour(openHours, 17)
            } else if (this.middleMonths.indexOf(currentMonth) != -1) {
                isClose = this.checkHour(openHours, 19)
            } else if (this.summerMonths.indexOf(currentMonth) != -1) {
                isClose = this.checkHour(openHours, 20)
            }
            return isClose;
        }
        return true;

    }
    checkHour(openHours, hourToCheck, testMinTime = false) {
        // console.log(openHours,openHours.open.time, hourToCheck, testMinTime);
        if (testMinTime) { // Sabbath 
            let openHourTime = parseInt(openHours.open.time.substring(0, 2)); //open time after sabbath
            // console.log('openHourTime', this.props.place.name, openHourTime, openHourTime >= hourToCheck);
            if (openHourTime >= hourToCheck) {
                return true;
            }
            return false;
        } else { //Friday
            let closeHourTime = parseInt(openHours.close.time.substring(0, 2)); //Close time in friday
            // console.log('closeHourTime', this.props.place.name, closeHourTime,closeHourTime <= hourToCheck);

            if (closeHourTime <= hourToCheck && closeHourTime > 6) { // open in sabbath night
                return true;
            }
            return false;
        }
    }
    getOpenHours(openingHoursData, weekDay) {
        let dayMatch = null;
        openingHoursData.forEach(function (item) {
            if (weekDay == 5 && item.close && item.close.day == weekDay) {
                dayMatch = item;
            } else if (weekDay == 6 && item.open && item.open.day == weekDay) {
                dayMatch = item;
            }
        })
        return dayMatch;
    }
    getSaturdayData() {
        let styleClass;
        switch (this.state.closeInSaturdayStatus) {
            case true: //Close in Saturday
                this.closeInFridayText = 'סגור בשבת';
                styleClass = 'text-success';
                break
            case false: //open in Saturday
                this.closeInFridayText = 'פתוח בשבת';
                styleClass = 'text-danger';
                break
            case undefined:
                this.closeInFridayText = 'אין מידע';
                styleClass = 'text-warning';
                break
            default:
                this.closeInFridayText = 'אין מידע';
                styleClass = 'text-warning';
        }
        return styleClass;
    }
    displayInMap() {
        this.props.displayPlaceInMap();
    }
    render() {
        let saturdayTextClass = this.getSaturdayData();

        if (this.props.place) {
            let place = this.props.place;
            let index = this.props.index;
            let placeClassName = place.displayInMap ? 'googlePlace active' : 'googlePlace disActive';
            // console.log(place, place.error);
            if (this.state.error) {
                this.closeInFridayText = 'חלה שגיאה!'
                saturdayTextClass += ' error';
            }

            return (
                <tr key={index} className={placeClassName} onClick={this.displayInMap.bind(this)}>
                    <th>{index + 1}</th>
                    <th>{place.name}</th>
                    <th>{place.formatted_address}</th>
                    <th className={saturdayTextClass} title={this.state.openInSaturdayHours}>{this.closeInFridayText}</th>
                    {/* <th>{this.state.openNow}</th> */}
                </tr>)
        } else {
            return ('')
        }

    }

}

//https://developers.google.com/maps/documentation/javascript/places