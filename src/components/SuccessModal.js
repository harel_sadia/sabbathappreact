import React from 'react';

function SuccessModal({ showModal, closeSuccessModal, newPlaceDetails }) {
    if (!newPlaceDetails || !showModal) { return (<div></div>) }
    return (
        <div className="modal" id="successModal" style={{ display: (showModal ? 'block' : 'none') }}>
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title text-success">העדכון התקבל בהצלחה!</h4>
                        <button type="button" className="close" onClick={closeSuccessModal}>&times;</button>
                    </div>
                    <div className="modal-body">
                        העסק: <b> {newPlaceDetails.name} </b> <br />
                        שכתובתו: <span>{newPlaceDetails.city}</span>
                        {newPlaceDetails.street && <span>, {newPlaceDetails.street}</span>}
                        {newPlaceDetails.house && <span>, {newPlaceDetails.house}</span>}
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-success" onClick={closeSuccessModal}>סגור</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default (SuccessModal);
