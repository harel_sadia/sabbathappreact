import React from 'react';

export default function HelpModal() {
    return (
        <div className="modal fade" id="helpModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">

                    <div className="modal-header">
                        <h4 className="modal-title">איתור עסקים שומרי שבת</h4>
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div className="modal-body">
                        <p> באתר הזה תוכלו לבדוק אם עסק מסוים שומר שבת או לא. </p>
                        <p>
                            כדי לחפש עסק מסוים, צריך לכל הפחות להכניס את שם העסק ואת העיר שבה העסק נמצא.  <br />
                            ניתן גם לחפש באופן כללי ולקבל מגוון תוצאות.
                        </p>
                        <p>
                            למשל, אם בשם העסק נכניס "קפה" ובעיר "ירושלים" ונלחץ על "חפש" נקבל רשימה של 20 עסקים של קפה בירושלים. <br />
                            כדי לקבל מידע על עסק מסוים, יש להכניס גם את שם הרחוב ומספר הבית,  לא על כל העסקים יש מידע.
                        </p>
                            התוצאות המוצגות מגיעות מדיווחים של משתמשים (מסומנים במסגרת ירוקה) או מידע הקיים ברשת. <br /><br />

                            בלחיצה על אחת התוצאות, אותה התוצאה תסומן במפה. הסימון יהיה על ידי המספר של התוצאה.  <br />
                            למשל בלחיצה על העסק השלישי ברשימת התוצאות, יסומן במפה המספר 3 במיקום של אותו העסק. <br /><br />
                      
                            ליצירת קשר ופרטים נוספים ניתן לפנות לדוא"ל: <br />
                        <p><a href="mailto:janer.solutions@gmail.com">janer.solutions@gmail.com</a></p>
                    </div>

                    <div className="modal-footer">
                        <div className="col-md-4">
                            <button type="button" className="btn btn-primary btn-block" data-dismiss="modal">סגור</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}
