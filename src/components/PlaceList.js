import React from 'react';

import { connect } from 'react-redux';
import GooglePlaceItem from './GooglePlaceItem';
import PlaceReportItem from './PlaceReportItem';

import { Types } from '../actions/Actions';

class PlaceListGoogle extends React.Component {
    apiKey = "AIzaSyDz_aQ2_PGAvoALbH9oHtg-YpmxmH1K0lg";
    constructor(props) {
        super(props)
    }
    displayPlaceInMap(place, placeType) {
        this.props.dispatch({ type: Types.DISPLAY_PLACE_IN_MAP, place, placeType });
    }
    setPlaceError(place, error) {
        this.props.dispatch({ type: Types.SET_PLACE_ERROR, place, error });
    }
    renderTableHeader() {
        return (
            <tr>
                <th>#</th>
                <th>שם</th>
                <th>כתובת</th>
                <th title="פתוח בשבת">פתוח בשבת</th>
                {/* <th title="פתוח עכשיו">פתוח עכשיו</th> */}
            </tr>
        )
    }

    renderGooglePlacesRows() {
        let self = this;
        let apiKey = this.apiKey;
        let googleFirstIndex = this.props.placeListReports.length;
        let placeListRows = this.props.placeListGoogle.map(function (place, index) {
            let currentIndex = googleFirstIndex + index;
            return (
                <GooglePlaceItem
                    apiKey={apiKey}
                    key={currentIndex}
                    index={currentIndex}
                    place={place}
                    displayPlaceInMap={self.displayPlaceInMap.bind(self, place, 'google')}
                    setPlaceError={self.setPlaceError.bind(self)}
                    baseUrl={self.props.baseUrl}
                />
            );
        });
        return placeListRows;
    }
    renderReportsPlacesRows() {
        let self = this;
        let placeListRows = this.props.placeListReports.map(function (place, index) {
            return (
                <PlaceReportItem
                    key={index}
                    index={index}
                    place={place}
                    displayPlaceInMap={self.displayPlaceInMap.bind(self, place, 'report')}
                />
            );
        });
        return placeListRows;
    }
    renderHtmlTable() {
        let placeListReportsRows = this.renderReportsPlacesRows();
        let placeListGoogelRows = this.renderGooglePlacesRows();
        // console.log(placeListGoogelRows, placeListReportsRows);
        let placeListTable = null;

        let tableHeader = this.renderTableHeader();
        placeListTable = <div>
            <h4 className="text-success titleStyle">על מנת להציג מיקום על המפה - לחץ עליו בטבלה:</h4>
            <div className="tableBox">
                <table className="table table-striped" >
                    <thead>
                        {tableHeader}
                    </thead>
                    <tbody>
                        {placeListReportsRows}
                        <tr className="separateRow"><td colSpan="4"></td></tr>
                        {placeListGoogelRows}
                    </tbody>
                </table>
            </div>
        </div>

        return placeListTable;
    }
    render() {
        let titleText = 'תוצאות חיפוש: ';
        let placeListTable = null;
        if (this.props.placeListGoogle.length > 0 || this.props.placeListReports.length > 0) {
            placeListTable = this.renderHtmlTable(titleText);
        } else {
            titleText = 'אין תוצאות!';
        }
        return (
            <div className="col-md-6 col-xs-12" >
                <div className="placeList">
                    <h1 style={{ width: '100%' }} className="text-warning titleStyle">{titleText}</h1>
                    {placeListTable}
                </div>
            </div>


        )
    }
}
const mapStateToProps = function (state) {
    return {
        placeListGoogle: state.placeList,
        placeListReports: state.placeListReports,
        baseUrl: state.baseUrl
    }
}
export default connect(mapStateToProps)(PlaceListGoogle);
