import React from 'react';
import { connect } from 'react-redux';
import { Types, fetchPlaces, searchPlaces } from '../actions/Actions';


const google = window.google;

class MapSearchPlaces extends React.Component {

    searchBox;
    map;
    infoWindow;
    markerList = [];

    componentWillMount() {
        var self = this;
        // this.mapRef = React.createRef();
        setTimeout(function () {
            self.initAutocomplete();
        }, 500);
        this.resetPosition = false;
    }
    componentWillReceiveProps(nextProps) {
        if (!this.props.displayInMap && nextProps.displayInMap) {
            this.printMapMarkers();
        }
        // console.log(nextProps.displayMap, this.props.displayMap);
        if (!this.props.displayMap && nextProps.displayMap) {
            this.resetPosition = true;
            this.printMapMarkers();
            // this.setMapLocation();
        } else {
            this.resetPosition = false;
        }
    }
    // focusOnMap() {
    //     this.mapRef.current.focus();
    // }
    initAutocomplete() {
        var self = this;
        let isrealCord = { lat: 31.75, lng: 35.2 };
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: isrealCord,
            zoom: 15,
            mapTypeId: 'roadmap'
        });
        this.initMapPosition();
        // Create the search box and link it to the UI element.
        let searchInput = document.getElementById('searchBox');
        searchInput.value = this.props.placeName || '';
        this.searchBox = new google.maps.places.SearchBox(searchInput);
        this.map.controls.push(searchInput);

        // Try to select the first place
        document.getElementById('submitBtn').onclick = function (e) {
        self.props.dispatch({ type: Types.SEARCH_FOR_PALCE,placeList: [], placeName:'' });

                var request = {
                    query: searchInput.value,
                    fields: ['place_id','formatted_address', 'name', 'rating', 'opening_hours', 'geometry'],
                };
                const service = new google.maps.places.PlacesService(self.map);
                service.textSearch(request, callback);
                function callback(placeList, status) {

                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        // console.log(placeList);
                     let placeName = searchInput.value;

                        self.onPlaceSearch(placeList, placeName)
                    }
                }
                // google.maps.event.trigger(self.searchBox, 'keypress', {  key: 13, char: 13 });
        };

        // Bias the SearchBox results towards current map's viewport.
        this.map.addListener('bounds_changed', function () {
            self.searchBox.setBounds(self.map.getBounds());
        });

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        this.searchBox.addListener('places_changed', function () {
            let placeList = self.searchBox.getPlaces();
            let placeName = searchInput.value;

            self.onPlaceSearch(placeList, placeName)
        });
    }
    onPlaceSearch(placeList, placeName) {
        let self = this;
        fetchPlaces(self.props.dispatch, self.props.searchDetails);
        self.props.dispatch({ type: Types.SEARCH_FOR_PALCE, placeList, placeName });
        if (placeList.length === 0) {
            return;
        }
        placeList.forEach(function (place) {
            place.type = 'google';
            place.displayInMap = false;
        });

        self.printMapMarkers();
    }
    printMapMarkers() {
        const self = this;
        let placeList = this.props.placeListReports.concat(this.props.placeListGoogle);
        // console.log(this.props.placeList, this.props.markerList);
        let markerList = [];
        // Clear out the old markers.
        this.props.markerList.forEach(function (marker) {
            marker.setMap(null);
        });
        // For each place, get the icon, name and location.
        let bounds = new google.maps.LatLngBounds();
        placeList.forEach(function (place, index) {
            if (!place.displayInMap || !place.geometry) {
                // console.log("Returned place contains no geometry");
                return;
            }
            self.addSingleMarker(markerList, place, index);
            if (place.type == 'google' && place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else if (place.geometry.location) {
                bounds.extend(place.geometry.location);
            }

        });
        this.props.dispatch({ type: Types.UPDATE_MARKER_LIST, markerList });

        this.map.fitBounds(bounds);
        if (markerList.length === 0) {
            this.setMapLocation();
        } else {
            if (this.resetPosition) {
                google.maps.event.trigger(self.map, 'resize');
                // self.map.setCenter(markerList[0].position);
                self.map.setZoom(15);
            }

        }
    }
    addSingleMarker(markerList, place, index) {
        if (!place.geometry.location) { return; }
        const self = this;

        let labelColor = place.type == 'report' ? 'white' : 'blue';
        let icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
        };
        let marker = new google.maps.Marker({
            map: self.map,
            icon: icon,
            title: place.name,
            position: place.geometry.location,
            label: {
                text: '' + (index + 1),
                color: labelColor,
                fontSize: "15px",
                fontWeight: "bold"
            },
        });
        // marker.addListener('click', function () {
        //     self.map.setZoom(8);
        //     self.map.setCenter(marker.getPosition());
        // });
        // Create a marker for each place.
        markerList.push(marker);
    }
    initMapPosition() {
        this.infoWindow = new google.maps.InfoWindow();
        this.setMapLocation();
    }
    handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'אירעה שגיאה, איתור המיקום נכשל' :
            'הדפדפן איננו תומך בשרות מיקום');
        infoWindow.open(this.map);
    }
    setMapLocation() {
        let self = this;
        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                self.infoWindow.setPosition(pos);
                self.infoWindow.setContent('...מיקומך');
                self.infoWindow.open(self.map);
                self.map.setCenter(pos);
                self.map.setZoom(15);
            }, function () {
                self.handleLocationError(true, self.infoWindow, self.map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            this.handleLocationError(false, this.infoWindow, self.map.getCenter());
        }
    }
    render() {

        return (
            <div id="map" style={{ 'width': '100%', 'height': '65vh', 'display': this.props.displayMap ? 'block' : 'none' }}></div>
        )
    }
}

const mapStateToProps = function (state) {
    return {
        placeListGoogle: state.placeList,
        placeListReports: state.placeListReports,
        placeName: state.placeName,
        markerList: state.markerList,
        displayInMap: state.displayInMap,
    }
}
export default connect(mapStateToProps)(MapSearchPlaces);
