
import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';

import store from '../store/store';
import { isMobile } from 'react-device-detect';

import Nav from '../Nav';
import * as Actions from "../actions/Actions";

import SuccessModal from './SuccessModal';
import HelpModal from './HelpModal';
import ContactModal from './ContactModal';

class ReportPlace extends Component {
  constructor(props) {
    super(props)

  }
  componentWillMount() {
    this.initialState = {
      userName: '',
      phone: '',
      name: '',
      city: '',
      street: '',
      house: '',
      status: '',
      more: 'פרטים נוספים',
    }
    /* this.initialState = { 
       userName: "שם", phone: "584671611", name: "הראל סעדיה", city: "ברכה",
      address: "שערי ברכה, 130/6", status: 1,more: 'פרטים נוספים' 
    }; */
    this.setState(this.initialState)
    this.notValidStyle = { border: '1px solid red' };
  }

  onInputChange(inputName, e) {
    // console.log(e);
    let obj = {};
    obj[inputName] = e.target.value;
    this.setState(obj);
  }
  clearPage() {
    this.setState({ ...this.initialState });
  }
  getFormDetails() {
    let formDetails = ['userName', 'phone', 'name', 'city', 'street', 'house', 'status', 'more'];
    let formObj = {};

    for (let i in formDetails) {
      let name = formDetails[i];
      formObj[name] = this.state[name].trim();
    }

    return formObj;
  }
  checkFormValidation() {
    let valid = true;
    let self = this;
    let requiredDetails = ['name', 'city', 'street', 'house', 'status'];
    requiredDetails.forEach(function (name) {
      if (!self.checkValidValue(name)) {
        valid = false;
      }
    });
    return !valid;
  }
  getInputStyle(field) {
    let fieldStyle = {};
    fieldStyle = this.checkValidValue(field) ? {} : this.notValidStyle;
    return fieldStyle;
  }
  checkValidValue(field) {
    let valid = true;

    switch (field) {
      case 'name':
        valid = this.checkValidInput(field)
        break;
      case 'city':
        valid = this.checkValidInput(field)
        break;
      case 'status':
        valid = this.checkValidInput(field, true)
        break;
    }
    return valid;

  }
  checkValidInput(field, isSelectField = false) {
    let valid = true;

    let value = this.state[field] || '';
    if (!isSelectField) {
      if (value.length < 2) { valid = false; }
    } else {
      if (value == '') { valid = false; }
    }

    return valid
  }

  onSubmit(e) {
    e.preventDefault();
    let formObj = this.getFormDetails();
    this.clearPage();
    // console.log(formObj);
    Actions.addPlace(formObj, store.dispatch);

  }
  closeSuccessModal() {
    store.dispatch({ type: Actions.Types.SHOW_SUCCESS_MODAL, showModal: false })
  }
  render() {

    let disabledSubmit = this.checkFormValidation();
    let formPadding = isMobile ? '5px' : '50px';
    let titleText = 'דיווח על עסק :';
    let titleClass = 'text-right text-success titleStyle';
    return (
      <div>
        <Nav></Nav>
        <div id="reportPage" className="container">
          <div className="row">
            <div className="col-md-12">
              <form style={{ padding: formPadding, textAlign: 'right', direction: 'rtl' }}>
                <h1 className={titleClass}>{titleText}</h1>

                <div className="row">
                  <div className="form-group col-md-4">
                    <span className="redMark">*</span>
                    <input value={this.state.name} onChange={this.onInputChange.bind(this, 'name')}
                      style={this.getInputStyle('name')}
                      type="text" className="form-control" id="name" placeholder="שם העסק" />
                  </div>

                  <div className="form-group col-md-4">{/*רשימת ערים?*/}
                    <span className="redMark">*</span>
                    <input value={this.state.city} onChange={this.onInputChange.bind(this, 'city')}
                      style={this.getInputStyle('city')}
                      type="text" className="form-control" id="city" placeholder="עיר" />
                  </div>

                  <div className="form-group col-md-4">
                    <span className="redMark">*</span>
                    <select value={this.state.status} onChange={this.onInputChange.bind(this, 'status')}
                      style={this.getInputStyle('status')}
                      className="form-control" id="status" >
                      <option value="" className="hide">בחר סטטוס</option>
                      <option value="1">שומר שבת</option>
                      <option value="0">לא שומר שבת</option>
                    </select>

                  </div>
                  <div className="form-group col-md-4 address">
                    <input value={this.state.street} onChange={this.onInputChange.bind(this, 'street')}
                      type="text" className="form-control" id="street" placeholder="רחוב" />
                  </div>

                  <div className="form-group col-md-4 address">
                    <input value={this.state.house} onChange={this.onInputChange.bind(this, 'house')}
                      type="text" className="form-control" id="house" placeholder="בית" />
                  </div>

                  <div className="form-group col-md-4">
                    <input value={this.state.userName} onChange={this.onInputChange.bind(this, 'userName')}
                      type="text" className="form-control" id="userName" placeholder="שם המדווח" />
                  </div>

                  <div className="form-group col-md-4">
                    <input value={this.state.phone} onChange={this.onInputChange.bind(this, 'phone')}
                      type="text" className="form-control" id="phone" placeholder="פלאפון" />
                  </div>

                  <div className="form-group col-md-12">
                    <textarea value={this.state.more} onChange={this.onInputChange.bind(this, 'more')}
                      rows="4" className="form-control" name="more"></textarea>
                  </div>
                </div>
                <h6 className="text-danger">*שדות חובה מסומנים באדום</h6>

                <div className="row">
                  <div className="col-md-4"></div>
                  <div className="form-group col-md-4 offset-md-2">
                    <button type="button" onClick={this.onSubmit.bind(this)} className="btn btn-success btn-block" disabled={disabledSubmit}>שלח</button>
                  </div>
                  <div className="form-group col-md-2 text-left">
                    <button type="button" className="btn btn-danger btn-md" onClick={this.clearPage.bind(this)} style={{ marginLeft: '10px' }}>נקה</button>
                    <Link to="/" className="btn btn-warning btn-md">חזור</Link>
                  </div>

                </div>
              </form>
              <SuccessModal
                showModal={this.props.showSuccessModal}
                newPlaceDetails={this.props.newPlaceDetails}
                closeSuccessModal={this.closeSuccessModal.bind(this)}
              />
            </div>
          </div>
        </div>
        <HelpModal/>

      </div>
    );
  }
}
const mapStateToProps = function (state) {
  return {
    newPlaceDetails: state.newPlaceDetails,
    showSuccessModal: state.showSuccessModal,
    baseUrl: state.baseUrl
  }
}
export default connect(mapStateToProps)(ReportPlace);
