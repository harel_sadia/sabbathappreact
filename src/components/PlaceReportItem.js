import React from 'react';
class PlaceReportItem extends React.Component {
    displayInMap() {
        this.props.displayPlaceInMap();
    }
    render() {
        const place = this.props.place;
        let index = this.props.index;
        let placeClassName = place.displayInMap ? 'reportPlace active' : 'reportPlace disActive';
        let placeStatus = place.status == 1 ? true : false;
        return (
            <tr key={index} className={placeClassName} onClick={this.displayInMap.bind(this)} >
                <td>{index + 1}</td>
                <td>{place.name}</td>
                <td>
                    <b>{place.city}</b> ,
                <span>{(place.street ? place.street : '')}</span>
                    <span>{(place.house ? ', ' + place.house : '')}</span></td>
                <td className={placeStatus? 'text-success' : 'text-danger'} ><b>{placeStatus ? 'סגור בשבת' : 'פתוח בשבת'}</b></td>
            </tr>
        )
    }

}
export default PlaceReportItem;