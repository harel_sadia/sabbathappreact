import React, { Component } from 'react';
import MapSearchPlaces from './MapSearchPlaces';
import PlaceList from './PlaceList';

import HelpModal from './HelpModal';
import ContactModal from './ContactModal';

import Nav from '../Nav';
import { isMobile, MobileView } from "react-device-detect";


class SearchPlaces extends Component {
  componentWillMount() {
    this.initState = {
      displayMobileMap: false,
      validForm: false,
      validCity: false,
      validName: false,
      name: '',
      city: '',
      street: '',
      house: '',
    }
    this.setState({ ...this.initState });
    this.searchRef = React.createRef();

  }
  displayMapInMobile() {
    this.setState({ displayMobileMap: !this.state.displayMobileMap });
  }
  inputChange(inputName, e) {
    let newState = { ...this.state };
    newState[inputName] = e.target.value;
    this.setState(newState);
    let self = this;
    setTimeout(() => {
      self.updateSearchDetails();
      let validForm = self.checkValidForm();
      self.updateForm(validForm);
    }, 100);
  }
  updateSearchDetails() {
    let searchDetails = {};
    let searchDetailsList = ['name', 'city', 'street', 'house']
    searchDetailsList.forEach(item => {
      searchDetails[item] = this.state[item].trim();
    });
    this.searchDetails = searchDetails;
  }
  checkValidForm() {
    let validName = false;
    let validCity = false;
    if (this.checkValidInput('name')) {
      validName = true;
    }
    if (this.checkValidInput('city')) {
      validCity = true;
    }
    let validForm = (validName && validCity);
    this.setState({ validCity: validCity, validName: validName, validForm: validForm })
    return validForm;
  }
  checkValidInput(inputName) {
    let validInput = true;
    if (this.state[inputName].length < 3) {
      validInput = false
    }
    return validInput;
  }
  updateForm(validForm) {

    if (validForm) {
      let searchBox = '';
      searchBox = this.state.name + ' ליד ' + this.state.city;
      if (this.checkValidInput('street')) {
        searchBox += ' ב' + this.state.street;
      }
      if (this.checkValidInput('house')) {
        searchBox += '  ' + this.state.house;
      }
      this.setState({ searchBox: searchBox })
    }
  }

  searchBoxChange(e) {
    if (this.state.validForm) {
      this.setState({ searchBox: e.target.value })
    }
  }
  onSearch(e) {
    this.searchRef.current.focus();
    /*
    setTimeout(function(){
      var e = $.Event("keypress", { key: 13, char: 13 });
      $('#searchBox').trigger(e);
      $(".pac-container .pac-item:first-child").click();
    },1000)
  */
  }
  render() {
    let displayMap = isMobile ? this.state.displayMobileMap : true;
    let btnText = !displayMap ? 'הצג מפה' : 'הסתר מפה';
    let notValidStyle = { border: '1px solid red' };

    return (
      <div>
        <Nav></Nav>
        <div className="row">
          <div className="col-md-6 col-xs-12">
            <h4 className='text-success pull-right' >חיפוש עסק:</h4>
            <div id="searchForm" className="form-inline">
              <input id="place" type="text" className="form-control" value={this.state.name}
                style={this.state.validName ? {} : notValidStyle}
                onChange={this.inputChange.bind(this, 'name')} placeholder="שם עסק" title='הקלד שם עסק' />

              <input id="city" type="text" className="form-control" value={this.state.city}
                style={this.state.validCity ? {} : notValidStyle}
                onChange={this.inputChange.bind(this, 'city')} placeholder="עיר" title='הקלד עיר' />

              <input id="street" type="text" className="form-control" value={this.state.street}
                onChange={this.inputChange.bind(this, 'street')} placeholder="רחוב" title='הקלד רחוב' />

              <input id="house" type="text" className="form-control" value={this.state.house}
                onChange={this.inputChange.bind(this, 'house')} placeholder="בית" title='הקלד מספר בית' />

              <input id="searchBox" ref={this.searchRef} type="text" className="form-control" disabled={!this.state.validForm}
                value={this.state.searchBox || ''} onChange={this.searchBoxChange.bind(this)} placeholder="ערך חיפוש" />
              <button aria-label="Search" id="submitBtn" type="submit" className="btn btn-primary" disabled={!this.state.validForm} onClick={this.onSearch.bind(this)}>חפש</button>

            </div>
            <MapSearchPlaces displayMap={displayMap} searchDetails={this.searchDetails}
              displayMapInMobile={this.displayMapInMobile.bind(this)} />
            <MobileView style={{ width: '100%' }}>
              <button style={{ margin: '10px 0 ' }} type="button" className="btn btn-success btn-block" onClick={this.displayMapInMobile.bind(this)} >{btnText}</button>
            </MobileView>
          </div>
          <PlaceList searchDetails={this.searchDetails} />
        </div>
        <HelpModal/>
        {/* <ContactModal/> */}
      </div>
    );
  }
}

export default SearchPlaces;
