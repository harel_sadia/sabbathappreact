import React from 'react';

export default function ContactModal() {
    return (
        <div className="modal fade" id="contactModal" role="dialog" aria-labelledby="contactModal" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title">משוב</h4>
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div className="modal-body">
                      <p> ליצירת קשר ופרטים נוספים ניתן לפנות לדוא"ל: </p>
                      <p><a href="mailto:janer.solutions@gmail.com">janer.solutions@gmail.com</a></p>
                    </div>
                    <div className="modal-footer">
                        <div className="col-md-4">
                            <button type="button" className="btn btn-primary btn-block" data-dismiss="modal">סגור</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
