import React from 'react';
import ReactDOM from 'react-dom';

const google = window.google;

export default class SearchBox extends React.Component {

  onPlacesChanged = () => {
    // console.log(this.searchBox.getPlaces());
    if (this.props.onPlacesChanged) {
      this.props.onPlacesChanged(this.searchBox.getPlaces());
    }
  }
  onBoundChanged = () => {
    // console.log(this.searchBox.getPlaces());
  }

  componentDidMount() {
    let self = this;
    setTimeout(function () {
      self.initAutocomplete();
    }, 500);
  }

  initAutocomplete() {
    let self = this;

    // var map = new google.maps.Map(document.getElementById('map'), {});

    // Create the search box and link it to the UI element.
    let input = ReactDOM.findDOMNode(this.refs.input);

    this.searchBox = new google.maps.places.SearchBox(input);

    // Bias the SearchBox results towards current map's viewport.
    // map.addListener('bounds_changed', function () {
    //   self.searchBox.setBounds(map.getBounds());
    //   console.log(map.getBounds());
    // });

    this.searchBox.addListener('bounds_changed', this.onBoundChanged);
    this.searchBox.addListener('places_changed', this.onPlacesChanged);
  }
  componentWillUnmount() {
    // https://developers.google.com/maps/documentation/javascript/events#removing
    google.maps.event.clearInstanceListeners(this.searchBox);
  }
  render() {
    return <div>
      <div id="map" style={{ display: 'none' }}></div>
        <input id="pac-input" ref="input" {...this.props} type="text"
          className="form-control" placeholder="שם עסק ו/או כתובת" title='הקלד שם עסק' />;
     </div>
  }
}