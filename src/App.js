import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux'
import store from './store/store';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import ReportPlace from './components/ReportPlace';
import SearchPlaces from './components/SearchPlaces';

import registerServiceWorker from "./registerServiceWorker";


class App extends Component {

  render() {
    return (
      <div className="appContainer container" style={{ direction: 'rtl', textAlign: 'right' }}>
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
              <Route path="/search/places" component={SearchPlaces} />
              <Route path="/report/place" component={ReportPlace} />
              <Route component={SearchPlaces} />
          </Switch>
        </BrowserRouter>
      </Provider>
      </div>
    );
  }
}

registerServiceWorker();

export default App;
