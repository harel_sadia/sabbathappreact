import placesRef from "../config/firebase";
import axios from 'axios';
import { DEV_MODE } from '../reducers/Reducers';

export const Types = {
  SEARCH_FOR_PALCE: 'SEARCH_FOR_PALCE',
  DISPLAY_PLACE_IN_MAP: 'DISPLAY_PLACE_IN_MAP',
  SET_PLACE_ERROR: 'SET_PLACE_ERROR',
  CLEAN_MARKER_LIST: 'CLEAN_MARKER_LIST',
  UPDATE_MARKER_LIST: 'UPDATE_MARKER_LIST',
  FETCH_ALL_PLACES: 'FETCH_ALL_PLACES',
  SHOW_SUCCESS_MODAL: 'SHOW_SUCCESS_MODAL',
}

function insertPlace(dispatch, placeDetails) {
  let d = new Date();
  placeDetails.date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  placeDetails.dev_mode = DEV_MODE;
  placesRef.push(placeDetails).then(res => {
    // console.log(res.getKey()) // this will return you ID
    dispatch({ type: Types.SHOW_SUCCESS_MODAL, showModal: true, newPlaceDetails: placeDetails })
    setTimeout(function () {
      dispatch({ type: Types.SHOW_SUCCESS_MODAL, showModal: false, newPlaceDetails: null })
    }, 10000)
  });
}
function getAddressCoordinates(dispatch, placeDetails) {
  let apiKey = 'AIzaSyDz_aQ2_PGAvoALbH9oHtg-YpmxmH1K0lg';
  let address = placeDetails.city;
  if (placeDetails.street != '') { address += '+' + placeDetails.street }
  if (placeDetails.house != '') { address += '+' + placeDetails.house }
  let fullAddress = address + ',+IL' //Can add name of place to url!
  let geocodeUrl = 'https://maps.googleapis.com/maps/api/geocode/json'
  axios({
    url: geocodeUrl,
    method: 'GET',
    params: { address: fullAddress, key: apiKey }
  }).then(function (resp) {
    // console.log(resp);

    if (resp.data.results && resp.data.results.length > 0) {
      let results = resp.data.results[0];
      placeDetails.place_id = results.place_id;
      placeDetails.geometry = results.geometry ? results.geometry : null;
    }
    // console.log(placeDetails);
    insertPlace(dispatch, placeDetails);
  }), function (error) {
    console.log(error);
  };

}
function checkDetail(searchDetails, currentPlace, name) {
  let value = searchDetails[name].trim();
  // console.log(value, currentPlace[name].trim());
  let valid = true;
  if (value.length > 0 && currentPlace[name].trim() != value) {
    valid = false;
  }
  return valid;
}
export const addPlace = function (newPlace, dispatch) {
  // console.log(newPlace);
  let placeDetails = JSON.parse(JSON.stringify(newPlace))
  getAddressCoordinates(dispatch, placeDetails);
};

export const fetchPlaces = function (dispatch, searchDetails) {
  // console.log(searchDetails);
  let query = placesRef.orderByChild("name").equalTo(searchDetails.name);
  query.on("value", function (snapshot) {
    let placeListData = snapshot.val();
    let placeList = [];
    // console.log(snapshot.val());
    for (let place_key in placeListData) {
      let currentPlace = placeListData[place_key]
      let displayPlace = (DEV_MODE || !currentPlace.dev_mode) ? true : false;
      if (displayPlace && checkDetail(searchDetails, currentPlace, 'city')) {
        currentPlace.place_key = place_key;
        currentPlace.type = 'report';
        currentPlace.displayInMap = false;
        placeList.push(currentPlace);
      }
    }
    dispatch({ type: Types.FETCH_ALL_PLACES, placeListReports: placeList });
  });
};

// https://developers.google.com/maps/documentation/geocoding/start
// https://maps.googleapis.com/maps/api/geocode/json?address=אריאל+ששת+הימים+6,IL&key=AIzaSyDz_aQ2_PGAvoALbH9oHtg-YpmxmH1K0lg
// let geocodeUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + ',+IL&key=' + apiKey
