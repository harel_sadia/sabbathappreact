import React from 'react';
import { Link } from 'react-router-dom';
import { isMobile } from 'react-device-detect';
import { connect } from 'react-redux';

function Nav() {
    let titleText = 'איתור עסקים שומרי שבת';
    let titleClass = 'text-primary text-center';
    let buttonsStyle = isMobile ? {} : { float: 'right' };
    const buttons = <div style={buttonsStyle}>

        <Link to="/search/places" ><button className="btn btn-primary btn-sm" type="button" >חיפוש עסק</button></Link>
        <Link to="/report/place" ><button className="btn btn-success btn-sm" type="button" >דיווח על עסק</button></Link>
        <a><button type="button" className="btn btn-outline-dark btn-sm" data-toggle="modal"
            data-target="#helpModal" style={{ marginRight: '10px'}}>עזרה</button></a>
    </div>
    let navClass = 'navbar navbar-light row';
    let blueStyle = { backgroundColor: '#e3f2fd' };
    // let greenStyle = { backgroundColor: '#7CFC00' };

    let mobileHtml = <div style={{ width: '100%' }}>
        <nav className={navClass} style={blueStyle}>
            <div className='col-sm-12' >
                {buttons}
            </div>
        </nav>
        <nav className={navClass} style={{ borderBottom: '1px solid #007bff' }} >
            <h3 className={titleClass + ' col-sm-12 no-padding'} >
                {titleText}
            </h3>
        </nav>
    </div>;

    let desktopHtml = <nav className={navClass} style={blueStyle}>
        <h1 className={titleClass + ' col-md-12'} style={{ paddingLeft: '186px' }}>
            {titleText}
            {buttons}
        </h1>
    </nav>

    return (
        <div className="row">
            <div className="col-md-12">
                {isMobile ? mobileHtml : desktopHtml}
            </div>
        </div>
    )
}

const mapStateToProps = function (state) {
    return {
        placeList: state.placeList
    }
}
export default connect(mapStateToProps)(Nav);
